# PRTG Notification For PagerDuty Events API v2

A Simple PowerShell based "Execute Program" notification for PRTG that uses PagerDuty's Event API v2


This notification type allows PRTG alerts to be sent to a PagerDuty instance through their "Events API v2".

Full instructions on how to add the notification to PRTG, and configure your PagerDuty instance to receive them, can be found in this KB article:

https://kb.paessler.com/en/topic/89522-how-can-i-use-the-pagerduty-events-api-v2-with-notifications-in-prtg

Additional documentation for PagerDuty's Events API v2 can be found here:

https://developer.pagerduty.com/docs/events-api-v2/overview/

The script supports both US and EU Pagerduty instances (see documentation).
